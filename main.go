package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"

	"gitlab.com/LukaPG/go-server/reader"
	"gitlab.com/LukaPG/go-server/writer"
)

func main() {
	var wg sync.WaitGroup

	http.HandleFunc("/numbers", func(w http.ResponseWriter, r *http.Request) {
		getNumbers(&wg, &w)
	})

	log.Fatal(http.ListenAndServe(":8080", nil))

}

func getNumbers(wg *sync.WaitGroup, w *http.ResponseWriter) {
	ch := make(chan uint64)

	fmt.Printf("Start reading data\n\n")

	wg.Add(1)
	go reader.ReadData(ch, wg)
	wg.Add(1)
	go writer.Print(ch, wg, *w)
	wg.Wait()
	fmt.Println("End reading data")
}
