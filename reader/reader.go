package reader

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)

var r = rand.New(rand.NewSource(time.Now().UnixNano()))

// ReadData - Reads from numbersApi and pushes numbers into the channel
func ReadData(ch chan<- uint64, wg *sync.WaitGroup) {
	defer func() {
		close(ch)
		wg.Done()
	}()

	url := "http://numbersapi.com/random/math"
	for i := 0; i < 10; i++ {
		resp, err := http.Get(url)

		if err != nil {
			fmt.Println("Http response fail.")
			ch <- r.Uint64()
			continue
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println("Response body bad.")
			ch <- r.Uint64()
			continue
		}

		bodyString := string(body)
		fmt.Printf("Response number: %s\n", bodyString)
		number := strings.Split(bodyString, " ")[0]
		res, err := strconv.Atoi(number)
		if err != nil {
			fmt.Println("Response number bad.")
			ch <- r.Uint64()
			continue
		}
		ch <- uint64(res)
		resp.Body.Close()
		time.Sleep(100 * time.Millisecond)
	}
}
