package writer

import (
	"net/http/httptest"
	"sync"
	"testing"
)

func TestWriter_WritesToHttp(t *testing.T) {
	// Arrange
	var wg sync.WaitGroup
	ch := make(chan uint64)
	recorder := httptest.NewRecorder()
	wg.Add(1)
	go func(chIn chan uint64, wg1 *sync.WaitGroup) {
		chIn <- 64
		chIn <- 128
		defer func() {
			close(chIn)
			wg1.Done()
		}()
	}(ch, &wg)

	// Act
	wg.Add(1)
	go Print(ch, &wg, recorder)
	wg.Wait()

	// Assert
	if recorder.Code < 200 || recorder.Code >= 300 {
		t.Errorf("Print did not write into the response correctly. Code is %d", recorder.Code)
	}
}
