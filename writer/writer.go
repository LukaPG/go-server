package writer

import (
	"fmt"
	"net/http"
	"sync"
	"time"
)

// Print - prints numbers from the channel to the console
func Print(chIn chan uint64, wg *sync.WaitGroup, w http.ResponseWriter) {
	defer wg.Done()

	for num := range chIn {
		fmt.Printf("Got %d out of channel at %s\n\n", num, time.Now())
		fmt.Fprintf(w, "%d\n", num)
	}
}
